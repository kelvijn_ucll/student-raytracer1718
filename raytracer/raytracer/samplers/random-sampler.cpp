#include "samplers/random-sampler.h"
#include <random>
#include <iostream>

using namespace math;
using namespace raytracer;


namespace
{
	class RandomSampler : public raytracer::samplers::_private_::SamplerImplementation
	{
	private:
		const int m_sample_count;

	public:

		RandomSampler(const int sample_count) : m_sample_count(sample_count) {}

		std::vector<math::Point2D> sample(const math::Rectangle2D& rectangle) const override {

			std::vector<Point2D> points;

			std::random_device rd;
			std::mt19937 gen(rd());
			std::uniform_real_distribution<> dis(0, 1);
			for (int n = 0; n < m_sample_count; n++) {
				const double x = dis(gen);
				const double y = dis(gen);

				points.push_back(rectangle.from_relative(Point2D(x, y)));
			}

			return points;
		}
	};
}

Sampler raytracer::samplers::random(const int sample_count)
{
	return Sampler(std::make_shared<RandomSampler>(sample_count));
}