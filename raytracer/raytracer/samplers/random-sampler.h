#pragma once

#include "samplers/sampler.h"


namespace raytracer
{
	namespace samplers
	{
		/// <summary>
		/// Creates a sampler that chooses random points
		/// </summary>
		Sampler random(const int sample_count);
	}
}