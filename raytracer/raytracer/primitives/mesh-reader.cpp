#include "primitives/mesh-reader.h"

#include "primitives/triangle-primitive.h"

#include "primitives/union-primitive.h"
#include <fstream>
#include <sstream>
#include "logging.h"
#include "easylogging++.h"

using namespace raytracer;
using namespace raytracer::primitives;
using namespace std;
using namespace math;

std::vector<Primitive> read_mesh(const std::string& path)
{
	LOG(INFO) << "Open bestand " << path;

	std::ifstream obj;
	obj.open(path, ios::in);

	if (!obj.is_open())
	{
		LOG(INFO) << "Geen .obj bestand";
		return std::vector<Primitive>();
	}
	//en.wikipedia.org/wiki/Wavefront_.obj_file
	LOG(INFO) << "Obj bestand geopend";

	std::vector<Point3D> vertices;
	std::vector<Primitive> triangles;
	std::string line;

	while (std::getline(obj, line))
	{
		std::istringstream stringStream(line);
		std::string method;

		stringStream >> method;
		//vertex?
		if (method == "v")
		{
			double x, y, z;

			// uit stream halen
			stringStream >> x >> y >> z;
			Point3D p(x, y, z);
			vertices.push_back(p);
		}
		//polygon??
		else if (method == "f")
		{
			int a, b, c;
			char s;

			std::vector<Point3D> polygon;

			while ((stringStream >> a >> s >> b >> s >> c) && s == '/')
			{
				// Vertices beginnen op -1 
				Point3D v(vertices[a - 1]);
				polygon.push_back(v);
			}

			int count = polygon.size();

			// polygon verdelen in driehoeken
			if (count >= 3)
			{
				triangles.push_back(triangle(polygon[0], polygon[1], polygon[2]));
			}
			if (count >= 4)
			{
				triangles.push_back(triangle(polygon[2], polygon[3], polygon[0]));
			}
			if (count >= 5)
			{
				triangles.push_back(triangle(polygon[3], polygon[4], polygon[0]));
			}
			if (count >= 6)
			{
				triangles.push_back(triangle(polygon[4], polygon[5], polygon[0]));
			}
		}
	}

	LOG(INFO) << "Done, obj bestand bestaat uit: , " << triangles.size() << " driehoeken in  " << vertices.size() << " vertexen";

	return triangles;
}

#include "primitives/bounding-box.h"
Primitive raytracer::primitives::mesh_obj(const std::string& path)
{

	Primitive mesh = make_union(read_mesh(path));
	Primitive x = primitives::bounding_box_accelerator(mesh);

	return x;
}