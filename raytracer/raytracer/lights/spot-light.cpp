#include "lights/spot-light.h"
using namespace math;
using namespace raytracer;
using namespace imaging;
namespace
{
	class SpotLight : public raytracer::lights::_private_::PointLightImplementation
	{
	public:
		SpotLight(const math::Point3D& beginPoint, const math::Point3D& endPoint, const math::Angle& angle, const imaging::Color& color)
			: PointLightImplementation(beginPoint), m_color(color), m_endPoint(endPoint), m_angle(angle) { }

	protected:
		LightRay cast_lightray_to(const math::Point3D& p) const override
		{
			const math::Ray ray(m_position, p);
			Vector3D u = (m_endPoint - m_position).normalized();
			Vector3D v = (p - m_position).normalized();
			if (u.dot(v) >= cos(m_angle / 2))
			{
				return LightRay(ray, m_color);
			}
			else
			{
				return LightRay(ray, colors::black());
			}
		}

	private:
		imaging::Color m_color;
		math::Point3D m_endPoint;
		Angle m_angle;
	};
}

LightSource raytracer::lights::spot(const math::Point3D& beginPoint, const math::Point3D& endPoint, const math::Angle& angle, const imaging::Color& color)
{
	return LightSource(std::make_shared<SpotLight>(beginPoint, endPoint, angle, color));
}