#pragma once
#include "lights/point-light.h"
#include "imaging/color.h"
#include "math/point.h"

namespace raytracer
{
	namespace lights
	{
		LightSource spot(const math::Point3D& beginPoint, const math::Point3D& endPoint, const math::Angle& angle, const imaging::Color& color);
	}
}
