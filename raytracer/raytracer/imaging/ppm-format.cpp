#include <bitset>
#include "imaging/ppm-format.h"

using namespace imaging;

namespace
{
	struct RGBColor
	{
		uint16_t r, g, b;

		RGBColor(const Color& c)
		{
			Color clamped = c.clamped();
			//bij uint8 rare beelden
			r = uint16_t(clamped.r * 255);
			g = uint16_t(clamped.g * 255);
			b = uint16_t(clamped.b * 255);
		}
	};
}


void imaging::write_text_ppm(const Bitmap& bitmap, std::ostream& out)
{

	//paulbourke.net/dataformats/ppm/
	int width = bitmap.width();
	int height = bitmap.height();

	out << "P3\n"
		<< width << " " << height << "\n"
		<< "255\n"
		;

	for (unsigned j = 0; j != height; j++)
	{
		for (unsigned i = 0; i != width; i++)
		{
			Color color(bitmap[Position2D(i, j)]);
			RGBColor rgb(color);
			out << rgb.r << " " << rgb.g << " " << rgb.b << "	";
		}
		out << "\n";
	}
}
