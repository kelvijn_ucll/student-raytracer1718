#include "raytracers/ray-tracer-v2.h"

using namespace imaging;
using namespace math;
using namespace raytracer;


TraceResult raytracer::raytracers::_private_::RayTracerV2::trace(const Scene& scene, const math::Ray& ray) const
{

	/*
	
		Determine the first positive hit between the ray and the scene.
		If there is no hit, return a TraceResult::no_hit.
		Initialize a Color result to colors::black(), i.e.with start with zero photons.
		Ask the hit.material for the MaterialProperties at the hit location.
		Compute the ambient lighting(as you did in v1) and add the color to result.
		Call process_lights, which will iterate over all lights.Add the return value(a color) to result.
		Return a TraceResult containing all necessary data, with as color result's final value.

		*/
		
	Hit hit;
	Color result = colors::black();
	
	if (scene.root->find_first_positive_hit(ray, &hit))
	{
		
		unsigned group_id = hit.group_id;
		MaterialProperties properties = hit.material->at(hit.local_position);
		Color result = colors::black();
		result = result + compute_ambient(properties);
		result = result + process_lights(scene, properties, hit, ray);
		return TraceResult(result, group_id, ray, hit.t);
	}
	else
	{
		return TraceResult::no_hit(ray);
	}
}
imaging::Color raytracer::raytracers::_private_::RayTracerV2::process_lights(const Scene& scene, const MaterialProperties& mProp, const Hit& hit, const math::Ray& ray) const
{
	/*
	
    Start with a fresh Color result set to black.
    Iterate over the scene's light sources. Call process_light_source for each light source and add the return value to result.
    Return result.

	*/
	Color result = colors::black();
	for (LightSource lightSource : scene.light_sources)
	{
		
		result = result + process_light_source(scene, mProp, hit, ray, lightSource);
	}
	return result;
}
imaging::Color raytracer::raytracers::_private_::RayTracerV2::process_light_source(const Scene& scene, const MaterialProperties& mProp, const Hit& hit, const math::Ray& ray, LightSource lightSource)const
{
	/*

	Initialize a local variable result of type Color to black.
	Ask the light source to enumerate all light rays that reach hit.position.
	Iterate over each of these light rays. Give each to process_light_ray. Add the return values to result.
	Return result.

	*/
	Color result = colors::black();
	Point3D point = ray.at(hit.t);
	std::vector<LightRay> incoming = lightSource->lightrays_to(point);
	for (LightRay lightray : incoming)
	{
		result = result + process_light_ray(scene, mProp, hit, ray, lightray);
	}
	return result;
}
imaging::Color raytracer::raytracers::_private_::RayTracerV2::process_light_ray(const Scene& scene, const MaterialProperties& mProp, const Hit& hit, const math::Ray& ray, const LightRay& lightRay)const
{
	/*

	Initialize local variable result to black.
	Call compute_diffuse and add its return value to result.
	Return result.

	*/
	Color result = colors::black();
	result = result + compute_diffuse(mProp, hit, ray, lightRay);
	return result;
}
imaging::Color raytracer::raytracers::_private_::RayTracerV2::compute_diffuse(const MaterialProperties& mProp, const Hit& hit, const math::Ray& ray, const LightRay& lightRay) const
{
	/*

	The direction of the incoming light.
	The light's color. 
	The position to be lit.
	The normal vector at the position to be lit.

	*/
	Point3D incoming = lightRay.ray.origin;
	Color lightsColor = lightRay.color;
	Color diffuseColor = mProp.diffuse;
	Point3D position = hit.position;
	Vector3D normalVector = hit.normal;
	Vector3D vector = (incoming - position).normalized();
	double dot = vector.dot(normalVector);

	if (dot <= 0) return colors::black();
	else
	{
		auto result = dot * lightsColor * diffuseColor;
		return result;
	}
}
raytracer::RayTracer raytracer::raytracers::v2()
{
	return raytracer::RayTracer(std::make_shared<raytracer::raytracers::_private_::RayTracerV2>());
}