#pragma once
#include "raytracers/ray-tracer.h"
#include "raytracers/ray-tracer-v4.h"
#include "math/ray.h"
#include <memory>

namespace raytracer
{
	namespace raytracers
	{
		namespace _private_
		{
			//vorige over nemen
			class RayTracerV5 : public raytracer::raytracers::_private_::RayTracerV4
			{
			public:
				TraceResult trace(const Scene&, const math::Ray&) const override;
			protected:
				virtual imaging::Color trace(const Scene&, const math::Ray&, double) const;
				//fix voor v6
				imaging::Color compute_reflection(const Scene&, const MaterialProperties&, const math::Ray&, const Hit&, double) const;
			};
		}
		RayTracer v5();
	}
}