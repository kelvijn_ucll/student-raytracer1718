#include "raytracers/ray-tracer-v4.h"
using namespace imaging;
using namespace math;
using namespace raytracer;
/*So, what do we actually need to do in ray 
tracer v4? Not much, really: we need only update process_light_ray so that it checks whether the given light ray intersects the scene at t values between 0 and 1. If not, it can continue 
in the same way as v3 did, otherwise it returns black.*/
imaging::Color raytracer::raytracers::_private_::RayTracerV4::process_light_ray(const Scene& scene, const MaterialProperties& mProp, const Hit& hit, const math::Ray& ray, const LightRay& lightRay) const
{
	Hit lightHit;
	scene.root->find_first_positive_hit(lightRay.ray, &lightHit);
	if (lightHit.t >= 0.0 && lightHit.t < 0.999)
	{
		return colors::black();
	}
	else
	{
		return RayTracerV3::process_light_ray(scene, mProp, hit, ray, lightRay);
	}
}
raytracer::RayTracer raytracer::raytracers::v4()
{
	return raytracer::RayTracer(std::make_shared<raytracer::raytracers::_private_::RayTracerV4>());
}