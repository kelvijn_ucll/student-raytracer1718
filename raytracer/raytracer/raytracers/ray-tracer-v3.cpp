#include "raytracers/ray-tracer-v3.h"
using namespace imaging;
using namespace math;
using namespace raytracer;

imaging::Color raytracer::raytracers::_private_::RayTracerV3::process_light_ray(const Scene& scene, const MaterialProperties& mProp, const Hit& hit, const math::Ray& ray, const LightRay& lightRay)const
{
	/*
	
    Call the base class's version of process_light_ray. Look online for how to do this in C++. Assign the result to a local variable result.
    Call compute_specular and add its return value to result.
    Return result.

	*/
	Color result = raytracer::raytracers::_private_::RayTracerV2::process_light_ray(scene, mProp, hit, ray, lightRay);
	result = result + compute_specular(mProp, hit, ray, lightRay);
	return result;
}
imaging::Color raytracer::raytracers::_private_::RayTracerV3::compute_specular(const MaterialProperties& mProp, const Hit& hit, const math::Ray& ray, const LightRay& lightRay) const
{
	/*
	You can gather all necessary information from the parameter values.
	To normalize vectors, you can use Vector3D::normalized.
	Reflection of a vector vector
	by a surface with normal normalVector
	is already implemented for you as Vector3D::reflect_by
	To compute ab
,	 use pow(a, b).
You can optimize the function a little bit by first checking whether the specular member of the given MaterialProperties is black. If it is, there's no point performing all calculations. 
	*/
	Color result;
	Point3D incoming = lightRay.ray.origin;
	Color lightsColor = lightRay.color;
	Color diffuseColor = mProp.specular;
	Point3D position = hit.position;
	Vector3D normalVector = hit.normal;
	Vector3D i = (position - incoming).normalized();
	auto r = i - 2 * (i.dot(normalVector)) * normalVector;
	Point3D eye = ray.origin;
	Vector3D vector = (eye - position).normalized();
	auto e = vector.reflect_by(normalVector);
	double cosa = vector.dot(r);
	if (cosa > 0)
	{
		result = lightsColor * diffuseColor * pow(cosa, mProp.specular_exponent);
	}
	else
	{
		result = colors::black();
	}
	return result;
}

raytracer::RayTracer raytracer::raytracers::v3()
{
	return raytracer::RayTracer(std::make_shared<raytracer::raytracers::_private_::RayTracerV3>());
}