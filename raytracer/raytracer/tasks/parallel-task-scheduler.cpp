#include "tasks/parallel-task-scheduler.h"
#include <thread>
#include <mutex>

using namespace tasks;


namespace
{
	class ParallelTaskScheduler : public tasks::schedulers::_private_::TaskSchedulerImplementation
	{
	public:
		void perform(std::vector<std::shared_ptr<Task>> tasks) const
		{
			static std::mutex mutex;
			std::vector<std::thread> threads;
			unsigned int num_threads = std::thread::hardware_concurrency();

			for (unsigned i = 0; i < num_threads; i++)
			{
				threads.push_back(std::thread(&ParallelTaskScheduler::execute, &tasks, &mutex));
			}

			for (auto &thread : threads)
			{
				thread.join();
			}
		}

		static void *execute(std::vector<std::shared_ptr<Task>>* tasks, std::mutex* mutex)
		{
			if (tasks->size() > 0)
			{
				mutex->lock();
				auto task = tasks->back();
				tasks->pop_back();
				mutex->unlock();
				task->perform();
				execute(tasks, mutex);
			}
			return 0;
		}
	};
}

TaskScheduler tasks::schedulers::parallel()
{
	return TaskScheduler(std::make_shared<ParallelTaskScheduler>());
}